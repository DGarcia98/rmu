package com.rmu.Mb;

import com.rmu.Dao.UsuarioDao;
import com.rmu.Entities.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

/**
 *
 * @author jorge.romerousam
 */
@ManagedBean
@ViewScoped
public class UsuarioMb {
    private Usuario usuario;
    private List<Usuario> usuarioList;
    private UsuarioDao udao;
    
    @PostConstruct
    public void init(){
    usuario = new Usuario();
    usuarioList = new ArrayList<Usuario>();
    udao =  new UsuarioDao();
    mostrar();
    
    }
    public void mostrar(){
        try {
            usuarioList = udao.mostrar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    public UsuarioDao getUdao() {
        return udao;
    }

    public void setUdao(UsuarioDao udao) {
        this.udao = udao;
    }
    
}
