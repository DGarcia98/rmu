package com.rmu.Dao;

import com.rmu.Entities.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jorge.romerousam
 */
public class UsuarioDao {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("rmuPU");
    EntityManager em = emf.createEntityManager();
    private Usuario usuario;
    private List<Usuario> usuarioList;

    public List<Usuario> mostrar() {

        try {
//            em.getTransaction().begin();
            usuarioList = new ArrayList<Usuario>();
            usuarioList = em.createNamedQuery("Usuario.findAll").getResultList();
//            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuarioList;
    }
}
