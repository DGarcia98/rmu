package com.rmu.Dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GenericDao {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("rmuPU");
    EntityManager em = emf.createEntityManager();

    public Object guardar(Object obj) {
        try {
            em.getTransaction().begin();
            em.persist(obj);
            em.flush();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
            return null;
        } finally {
            em.close();
            emf.close();
        }
        return obj;
    }
    
    public Object actualizar(Object obj) {
        try {
            em.getTransaction().begin();
            em.merge(obj);
            em.flush();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
            return null;
        } finally {
            em.close();
            emf.close();
        }
        return obj;
    }
}
