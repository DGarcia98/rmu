/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rmu.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jorge.romerousam
 */
@Entity
@Table(name = "horario_alumno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HorarioAlumno.findAll", query = "SELECT h FROM HorarioAlumno h")
    , @NamedQuery(name = "HorarioAlumno.findById", query = "SELECT h FROM HorarioAlumno h WHERE h.id = :id")})
public class HorarioAlumno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "nie_alumno", referencedColumnName = "nie")
    @ManyToOne
    private Alumno nieAlumno;
    @JoinColumn(name = "id_materia", referencedColumnName = "id")
    @ManyToOne
    private Materia idMateria;
    @JoinColumn(name = "id_horario", referencedColumnName = "id")
    @ManyToOne
    private Horario idHorario;

    public HorarioAlumno() {
    }

    public HorarioAlumno(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Alumno getNieAlumno() {
        return nieAlumno;
    }

    public void setNieAlumno(Alumno nieAlumno) {
        this.nieAlumno = nieAlumno;
    }

    public Materia getIdMateria() {
        return idMateria;
    }

    public void setIdMateria(Materia idMateria) {
        this.idMateria = idMateria;
    }

    public Horario getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Horario idHorario) {
        this.idHorario = idHorario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorarioAlumno)) {
            return false;
        }
        HorarioAlumno other = (HorarioAlumno) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rmu.Entities.HorarioAlumno[ id=" + id + " ]";
    }
    
}
