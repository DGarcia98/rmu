/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rmu.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jorge.romerousam
 */
@Entity
@Table(name = "encargado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encargado.findAll", query = "SELECT e FROM Encargado e")
    , @NamedQuery(name = "Encargado.findById", query = "SELECT e FROM Encargado e WHERE e.id = :id")
    , @NamedQuery(name = "Encargado.findByApellidos", query = "SELECT e FROM Encargado e WHERE e.apellidos = :apellidos")
    , @NamedQuery(name = "Encargado.findByNombres", query = "SELECT e FROM Encargado e WHERE e.nombres = :nombres")
    , @NamedQuery(name = "Encargado.findByDui", query = "SELECT e FROM Encargado e WHERE e.dui = :dui")
    , @NamedQuery(name = "Encargado.findByTelFijo", query = "SELECT e FROM Encargado e WHERE e.telFijo = :telFijo")
    , @NamedQuery(name = "Encargado.findByTelCelular", query = "SELECT e FROM Encargado e WHERE e.telCelular = :telCelular")
    , @NamedQuery(name = "Encargado.findByEmail", query = "SELECT e FROM Encargado e WHERE e.email = :email")
    , @NamedQuery(name = "Encargado.findByEstadoCivil", query = "SELECT e FROM Encargado e WHERE e.estadoCivil = :estadoCivil")})
public class Encargado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "apellidos")
    private String apellidos;
    @Size(max = 100)
    @Column(name = "nombres")
    private String nombres;
    @Size(max = 20)
    @Column(name = "dui")
    private String dui;
    @Size(max = 15)
    @Column(name = "tel_fijo")
    private String telFijo;
    @Size(max = 15)
    @Column(name = "tel_celular")
    private String telCelular;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Correo electrónico no válido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 250)
    @Column(name = "email")
    private String email;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 20)
    @Column(name = "estado_civil")
    private String estadoCivil;
    @JoinColumn(name = "nie_estudiante", referencedColumnName = "nie")
    @ManyToOne
    private Alumno nieEstudiante;

    public Encargado() {
    }

    public Encargado(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getTelFijo() {
        return telFijo;
    }

    public void setTelFijo(String telFijo) {
        this.telFijo = telFijo;
    }

    public String getTelCelular() {
        return telCelular;
    }

    public void setTelCelular(String telCelular) {
        this.telCelular = telCelular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Alumno getNieEstudiante() {
        return nieEstudiante;
    }

    public void setNieEstudiante(Alumno nieEstudiante) {
        this.nieEstudiante = nieEstudiante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Encargado)) {
            return false;
        }
        Encargado other = (Encargado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rmu.Entities.Encargado[ id=" + id + " ]";
    }
    
}
