package com.rmu.Entities;

import com.rmu.Entities.HorarioAlumno;
import com.rmu.Entities.Materia;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(Horario.class)
public class Horario_ { 

    public static volatile SingularAttribute<Horario, Date> horaInicial;
    public static volatile SingularAttribute<Horario, Materia> idMateria;
    public static volatile SingularAttribute<Horario, Date> horaFinal;
    public static volatile SingularAttribute<Horario, Integer> id;
    public static volatile ListAttribute<Horario, HorarioAlumno> horarioAlumnoList;

}