package com.rmu.Entities;

import com.rmu.Entities.Encargado;
import com.rmu.Entities.HorarioAlumno;
import com.rmu.Entities.Matricula;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(Alumno.class)
public class Alumno_ { 

    public static volatile SingularAttribute<Alumno, String> apellidos;
    public static volatile SingularAttribute<Alumno, Date> fechaNacimiento;
    public static volatile SingularAttribute<Alumno, String> direccion;
    public static volatile ListAttribute<Alumno, Matricula> matriculaList;
    public static volatile SingularAttribute<Alumno, String> estadoCivil;
    public static volatile SingularAttribute<Alumno, Integer> nie;
    public static volatile ListAttribute<Alumno, HorarioAlumno> horarioAlumnoList;
    public static volatile SingularAttribute<Alumno, String> nombres;
    public static volatile ListAttribute<Alumno, Encargado> encargadoList;
    public static volatile SingularAttribute<Alumno, byte[]> foto;
    public static volatile SingularAttribute<Alumno, String> genero;
    public static volatile SingularAttribute<Alumno, String> dui;
    public static volatile SingularAttribute<Alumno, String> telFijo;
    public static volatile SingularAttribute<Alumno, String> telCelular;
    public static volatile SingularAttribute<Alumno, String> email;

}