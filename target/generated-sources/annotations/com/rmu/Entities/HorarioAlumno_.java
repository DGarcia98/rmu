package com.rmu.Entities;

import com.rmu.Entities.Alumno;
import com.rmu.Entities.Horario;
import com.rmu.Entities.Materia;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(HorarioAlumno.class)
public class HorarioAlumno_ { 

    public static volatile SingularAttribute<HorarioAlumno, Alumno> nieAlumno;
    public static volatile SingularAttribute<HorarioAlumno, Horario> idHorario;
    public static volatile SingularAttribute<HorarioAlumno, Materia> idMateria;
    public static volatile SingularAttribute<HorarioAlumno, Integer> id;

}