package com.rmu.Entities;

import com.rmu.Entities.Carrera;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(CategoriaCarrera.class)
public class CategoriaCarrera_ { 

    public static volatile ListAttribute<CategoriaCarrera, Carrera> carreraList;
    public static volatile SingularAttribute<CategoriaCarrera, Integer> id;
    public static volatile SingularAttribute<CategoriaCarrera, String> nombre;

}