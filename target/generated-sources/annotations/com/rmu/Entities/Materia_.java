package com.rmu.Entities;

import com.rmu.Entities.Horario;
import com.rmu.Entities.HorarioAlumno;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(Materia.class)
public class Materia_ { 

    public static volatile SingularAttribute<Materia, Integer> id;
    public static volatile SingularAttribute<Materia, String> nombre;
    public static volatile ListAttribute<Materia, HorarioAlumno> horarioAlumnoList;
    public static volatile SingularAttribute<Materia, Integer> cupo;
    public static volatile ListAttribute<Materia, Horario> horarioList;

}