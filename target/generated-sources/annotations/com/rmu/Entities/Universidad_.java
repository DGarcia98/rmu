package com.rmu.Entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(Universidad.class)
public class Universidad_ { 

    public static volatile SingularAttribute<Universidad, String> direccion;
    public static volatile SingularAttribute<Universidad, byte[]> logo;
    public static volatile SingularAttribute<Universidad, String> tel;
    public static volatile SingularAttribute<Universidad, Integer> id;
    public static volatile SingularAttribute<Universidad, String> nombre;
    public static volatile SingularAttribute<Universidad, String> email;

}