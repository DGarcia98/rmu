package com.rmu.Entities;

import com.rmu.Entities.Alumno;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(Encargado.class)
public class Encargado_ { 

    public static volatile SingularAttribute<Encargado, String> apellidos;
    public static volatile SingularAttribute<Encargado, Alumno> nieEstudiante;
    public static volatile SingularAttribute<Encargado, String> direccion;
    public static volatile SingularAttribute<Encargado, String> dui;
    public static volatile SingularAttribute<Encargado, String> estadoCivil;
    public static volatile SingularAttribute<Encargado, Integer> id;
    public static volatile SingularAttribute<Encargado, String> telFijo;
    public static volatile SingularAttribute<Encargado, String> telCelular;
    public static volatile SingularAttribute<Encargado, String> email;
    public static volatile SingularAttribute<Encargado, String> nombres;

}