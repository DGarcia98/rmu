package com.rmu.Entities;

import com.rmu.Entities.Alumno;
import com.rmu.Entities.Carrera;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(Matricula.class)
public class Matricula_ { 

    public static volatile SingularAttribute<Matricula, String> tipoMatricula;
    public static volatile SingularAttribute<Matricula, Carrera> idCarrera;
    public static volatile SingularAttribute<Matricula, Alumno> nieAlumno;
    public static volatile SingularAttribute<Matricula, Boolean> solvente;
    public static volatile SingularAttribute<Matricula, Date> fechaMatricula;
    public static volatile SingularAttribute<Matricula, Integer> id;

}