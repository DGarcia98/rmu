package com.rmu.Entities;

import com.rmu.Entities.CategoriaCarrera;
import com.rmu.Entities.Matricula;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-07-25T11:34:58")
@StaticMetamodel(Carrera.class)
public class Carrera_ { 

    public static volatile ListAttribute<Carrera, Matricula> matriculaList;
    public static volatile SingularAttribute<Carrera, String> duracion;
    public static volatile SingularAttribute<Carrera, Integer> id;
    public static volatile SingularAttribute<Carrera, CategoriaCarrera> idCategoria;
    public static volatile SingularAttribute<Carrera, String> nombre;

}